import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor{

  constructor(public sessionService: SessionService) {

  }

  //once we have a session we want to make sure it is included on future requests
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        "Access-Control-Allow-Origin": '*',
        "Access-Control-Allow-Headers": 'Origin, X-Requested-With, Content-Type, Accept, Session',
        "Access-Control-Allow-Methods": 'GET, PUT, POST, DELETE, OPTIONS',
        Session: `${this.sessionService.getSession()}`
      }
    });
    return next.handle(request);
  }
}
