import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SessionService {
  private loggedIn: boolean = false;
  public session: string = '';
  sessionKey: string = 'SESSION';

  constructor(httpClient: HttpClient) {
    this.session = sessionStorage.getItem(this.sessionKey) || '';
  }

  setLoggedIn(loggedIn:boolean){
    this.loggedIn = loggedIn;
  }

  isLoggedIn(){
    return this.loggedIn;
  }

  setSession(session: string) {
    this.session = session;
    sessionStorage.setItem(this.sessionKey, session);
  }

  endSession() {
    sessionStorage.removeItem(this.sessionKey);
    this.loggedIn = false;
  }

  getSession(): string {
    return this.session;
  }
}
