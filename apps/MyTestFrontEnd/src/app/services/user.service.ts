import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  baseUrl: string = environment.restBaseUrl || "http://localhost/rest";

  constructor(public httpClient: HttpClient) {}

  login(user: User) {
    return this.httpClient.post(this.baseUrl + '/login', user);
  }
}
