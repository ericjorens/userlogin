import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router} from '@angular/router';
import { SessionService } from '../services/session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private sessionService:SessionService, private router:Router){
  }

  //Stops the page form being interacted with
  canActivate() {
    return this.loggedInAuth();
  }

  //Stops the page from loading
  canLoad(){
    return this.loggedInAuth();
  }

  loggedInAuth(){
    //redirect to another page before returning false
    if(!this.sessionService.isLoggedIn()){
      this.router.navigate(['']);
    }
    return this.sessionService.isLoggedIn();
  }

}
