import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(public sessionService:SessionService, private router: Router) { }

  ngOnInit(): void {
  }

  logout(){
    this.sessionService.endSession();
    this.router.navigate(['']);
  }
}
