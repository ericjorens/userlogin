import { Component, OnInit } from '@angular/core';
import { SessionResponse } from 'src/app/models/session-response.model';
import { User } from 'src/app/models/user.model';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  //the user form template
  user: User = { id: 1, name: '', password: '' };

  constructor(
    private userService: UserService,
    public sessionService: SessionService
  ) {}

  ngOnInit(): void {}

  /**
   * Subscribes to the session observable as provided by the http request.
   * Once the session information is available, store it in our session service
   */
  login() {
    this.userService.login(this.user).subscribe((res: SessionResponse) => {
      let loggedIn = res.auth || false;
      if(!loggedIn) {
        this.clearForm();
        alert("Please use a correct user login!");
      } else {
        // only set the session if the user is authorized
        if(!!res.session) {
          this.sessionService.setSession(res.session);
          this.sessionService.setLoggedIn(loggedIn);
        }
      }
    });


  }

  logout(){
    this.sessionService.endSession();
    this.clearForm();
  }

  clearForm(){
    this.user = Object.assign({ id: 1, name: '', password: '' });
  }
}
