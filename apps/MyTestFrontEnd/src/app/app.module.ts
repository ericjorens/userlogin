import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';

import { appRouting } from './app.routing';
import { UserService } from './services/user.service';
import { SessionService } from './services/session.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './services/auth-interceptor.service';
import { UserComponent } from './components/user/user.component';
import { AuthGuard } from './auth/auth.guard';

@NgModule({
  declarations: [AppComponent, LoginComponent, UserComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule, appRouting],
  providers: [
    UserService,
    SessionService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
