import { ModuleWithProviders } from '@angular/compiler/src/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { LoginComponent } from "./components/login/login.component";
import { UserComponent } from './components/user/user.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'loggedIn', component: UserComponent, canLoad: [AuthGuard], canActivate:[AuthGuard]},
  { path: '**', redirectTo: '' },
  { path: '**/**', redirectTo: '' }
];

export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
