export interface SessionResponse {
  auth?: boolean;
  session?: string;
}
