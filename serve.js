require('dotenv').config();
// import express
const express = require('express');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 3000;

// other imports
const bodyParser = require('body-parser');
const multer = require('multer');
const helmet = require('helmet');
const session = require('cookie-session');

const fs = require("fs");

app.use(cors({
    credentials: true
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(helmet()); // helment locks down the request headers


// set the session
var expiryDate = new Date(Date.now() + 60 * 60 * 1000); // this sets a time one hour from now
app.use(session({
    name: 'Session',
    keys: ['secret1', 'secret2'],
    cookie: {
        httpOnly: true,
        domain: 'http://localhost:4200',
        expires: expiryDate
    }
}));

//CLIENT
// statically serve our compiled Angular application(s)
// app.use("/", express.static("./serve/MyTestFrontEnd"));

//set up our headers
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Session, Access-Control-Allow-Origin, Access-Control-Allow-Methods");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    next();
});

//SERVER
//setting up our node application routes
var restBaseUrl = '/rest';
require('./src/routes/users.route')(app, fs, restBaseUrl);

// this creates the app and listens to the port
app.listen(port, () =>{
    console.log(`${ process.env.APP_NAME || 'Placeholder' } has started on port: ${ port }`)
});

