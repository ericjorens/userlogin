var sessionPath = '/routes/models/sessions.json';

module.exports = {
    checkSignIn: function(util, fs, req, res) {
        var sessionKey = req.header("Session");
        util.readFile(fs, __dirname + sessionPath).then(function (data) {
            var sessions = JSON.parse(data);
            for (var session in sessions) {
                if (sessions.hasOwnProperty(session)) {
                    let s = sessions[session];
                    if (s.hasOwnProperty("key") && s["key"] === sessionKey) {
                        return true;
                    }
                }
            }
            res.end(JSON.stringify({message:"Invalid Session"}));
        });
    }
}