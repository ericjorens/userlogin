module.exports = {

    // this is a mock db table read
    readFile: function(fs, srcPath) {
        return new Promise(function (resolve, reject) {
            fs.readFile(srcPath, 'utf8', function (err, data) {
                if (err) {
                    console.log("FILE READ ERR: " + srcPath);
                    reject(err)
                } else {
                    console.log("FILE READ OK: " + srcPath);
                    resolve(data);
                }
            });
        })
    },

    // this is a mock db table write
    writeFile: function(fs, savPath, data) {
        return new Promise(function (resolve, reject) {
            fs.writeFile(savPath, data, function (err) {
                if (err) {
                    console.log("FILE WRITE ERR: " + savPath);
                    reject(err)
                } else {
                    console.log("FILE WRITE OK: " + savPath);
                    resolve();
                }
            });
        })
    }
}