module.exports = function (app, fs, baseUrl) {
  var crypto = require("crypto");
  var util = require("../util/file.util");
  var session = require("../util/session.util");
  const cors = require('cors');

  var modelPath = "/models/users.json";
  var sessionPath = "/models/sessions.json";

  var generate_key = function () {
    var sha = crypto.createHash("sha256");
    sha.update(Math.random().toString());
    return sha.digest("hex");
  };


  app.options('/login', function (req, res) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', '*');
    res.setHeader("Access-Control-Allow-Headers", "*");
    res.end();
  });

  //apply a route to express
  app.post(baseUrl + "/login", function (req, res) {
    if (!req.body.name || !req.body.password) {
      res.end(
        JSON.stringify({
          status: 302,
          message: "name and password are both required!",
        })
      );
    } else {
      var valid = false;
      util.readFile(fs, __dirname + modelPath).then(function (data) {
        var users = JSON.parse(data);
        var userResponse = [];
        for (var x in users)
          if (users.hasOwnProperty(x)) {
            userResponse.push(users[x]);
          }
        userResponse.filter(function (user) {
          if (
            user.name === req.body.name &&
            user.password === req.body.password
          ) {
            valid = true;
          }
        });
        saveSession(valid, res);
      });
    }
  });

  app.get(baseUrl + "/logout", function (req, res) {
    var sessionKey = req.header("Session");
    // console.log(JSON.stringify(sessionKey));
    util.readFile(fs, __dirname + sessionPath).then(function (data) {
      var sessions = JSON.parse(data);
      for (var session in sessions) {
        if (sessions.hasOwnProperty(session)) {
          let s = sessions[session];
          if (s.hasOwnProperty("key") && s["key"] === sessionKey) {
            // console.log("DELETING SESSION: " + "session" + s.id.toString());
            delete sessions["session" + s.id.toString()];
          }
        }
      }
      // write the file
      return util
        .writeFile(fs, __dirname + sessionPath, JSON.stringify(sessions))
        .then(function () {
          res.end(JSON.stringify(true));
        });
    });
  });

  //calculates how old a session is
  var calculateAge = function (date) {
    var ageDifMs = Date.now() - date.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  };

  //saves the user session
  //deletes sessions that are greater than an hour old
  var saveSession = function (valid, res) {
    return new Promise(function (resolve, reject) {
      var sessionKey = generate_key();
      util.readFile(fs, __dirname + sessionPath).then(function (data) {
        var sessions = JSON.parse(data);

        // remove the sessions that are expired
        for (var x in sessions) {
          if (sessions.hasOwnProperty(x)) {
            let s = sessions[x];
            let sessionDate = new Date(s.ts);
            if (calculateAge(sessionDate) > 1) {
              delete sessions["session" + s.id];
            }
          }
        }

        // add the current session
        var num = Object.keys(sessions).length + 1;
        var session = { id: num, key: sessionKey, ts: new Date() };
        sessions["session" + num] = session;

        // write the data
        util
          .writeFile(fs, __dirname + sessionPath, JSON.stringify(sessions))
          .then(function () {
            res.end(JSON.stringify({ auth: valid, session: sessionKey }));
            resolve(true);
          });
      });
    });
  };
};
